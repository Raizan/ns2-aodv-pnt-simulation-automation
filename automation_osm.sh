#!/bin/bash
# Author: Raizan (https://github.com/Raizan)
#------------------------- ENV VARIABLES ---------------------------------------
TA=/home/reisuke/TugasAkhir
NS=$TA/ns-allinone-2.35/ns-2.35
SUMO_HOME=$TA/sumo-0.25.0
SCENARIOS=/mnt/local_disk_e/TugasAkhir/scenarios
DATABASE=$SCENARIOS/simulation.db
AODV_ORIGINAL_DIR=$NS/aodv_original
AODV_PNT_DIR=$NS/aodv_pnt
#------------------------- DB CREATION ----------------------------------------
# Check if DB exists
if [[ -e $SCENARIOS/simulation.db ]]; then
  echo "Database already exist"
else
  echo "Database does not exist"
  echo "Creating simulation.db..."
  sqlite3 $DATABASE "create table grid_results \
   (id INTEGER PRIMARY KEY, folder_name TEXT, category INTEGER, \
    aodv_pdr REAL, pnt_pdr REAL, \
    aodv_e2ed REAL, pnt_e2ed REAL, \
    aodv_ro INTEGER, pnt_ro INTEGER, \
    aodv_rreq_count INTEGER, pnt_rreq_count INTEGER \
   );"
  sqlite3 $DATABASE "create table osm_results \
   (id INTEGER PRIMARY KEY, folder_name TEXT, category TEXT, \
    aodv_pdr REAL, pnt_pdr REAL, \
    aodv_e2ed REAL, pnt_e2ed REAL, \
    aodv_ro INTEGER, pnt_ro INTEGER, \
    aodv_rreq_count INTEGER, pnt_rreq_count INTEGER \
   );"
   sqlite3 $DATABASE "create table sequences \
    ( grid_last_sequence INTEGER, osm_last_sequence INTEGER \
   );"
   sqlite3 $DATABASE "insert into sequences values \
    ( 0, 0 );"
  echo "Database created at $DATABASE"
fi

# Revert placeholders
# Replace aodv.cc with aodv.cc.bak
echo "Resetting parameters..."
yes | cp -rf $AODV_PNT_DIR/aodv.cc.bak $AODV_PNT_DIR/aodv.cc
# Replace template.tcl with template.tcl.bak
yes | cp -rf $TA/wizards/template.tcl.bak $TA/wizards/template.tcl

echo "Welcome to NS2 AODV & AODV-PNT SIMULATION AUTOMATION SCRIPT"

read -p "How many scenarios? " num_scenarios
read -p "How many nodes? " num_nodes

# ------------------------------ OSM ------------------------------------------
if [[ $num_nodes -eq 50 ]]; then
  SAVEDIRBASE=$SCENARIOS/osm/50nodes

elif [[ $num_nodes -eq 100 ]]; then
  SAVEDIRBASE=$SCENARIOS/osm/100nodes

elif [[ $num_nodes -eq 150 ]]; then
  SAVEDIRBASE=$SCENARIOS/osm/150nodes

elif [[ $num_nodes -eq 200 ]]; then
  SAVEDIRBASE=$SCENARIOS/osm/200nodes
fi

TYPE_FILE=$TA/wizards/specification.typ.xml
OSM_FILE=$TA/wizards/osm_maps/polisi_istimewa.osm

end_time=360                    # simulation end time

cbrstart=200.0
cbrstop=350.0
cbrsize=512                     # 512 Bytes
cbrrate=2KB                     # 2 KB
cbrinterval=1                   # 1 packet per second

# Map = polisi_istimewa
# Sender pos
SRC_X=515.89
SRC_Y=522.96
# Receiver pos,
DST_X=906.46
DST_Y=737.20

# Set sender and receiver pos in template.tcl
sed -i "s/\${SRC_X}/$SRC_X/" $TA/wizards/template.tcl;
sed -i "s/\${SRC_Y}/$SRC_Y/" $TA/wizards/template.tcl;
sed -i "s/\${DST_X}/$DST_X/" $TA/wizards/template.tcl;
sed -i "s/\${DST_Y}/$DST_Y/" $TA/wizards/template.tcl;

# Set other simulation parameters in template.tcl
sed -i "s/\${num_nodes}/$num_nodes/" $TA/wizards/template.tcl;
sed -i "s/\${cbrsize}/$cbrsize/" $TA/wizards/template.tcl;
sed -i "s/\${cbrrate}/$cbrrate/" $TA/wizards/template.tcl;
sed -i "s/\${cbrinterval}/$cbrinterval/" $TA/wizards/template.tcl;
sed -i "s/\${end_time}/$end_time/" $TA/wizards/template.tcl;

# Set DST_X and DST_Y in aodv_pnt/aodv.cc
sed -i "s/\${DST_X}/$DST_X/" $AODV_PNT_DIR/aodv.cc;
sed -i "s/\${DST_Y}/$DST_Y/" $AODV_PNT_DIR/aodv.cc;

osm_last_sequence=$(sqlite3 $DATABASE "select osm_last_sequence from sequences;");

for i in $(seq $(($osm_last_sequence + 1)) $(($osm_last_sequence + $num_scenarios)));
do
  # Increment osm sequence table
  sqlite3 $DATABASE "update sequences set osm_last_sequence = $i";
  # Define directory and create
  SAVEDIR="$SAVEDIRBASE/$i";
  mkdir $SAVEDIR;

  # Convert OSM map to SUMO
  netconvert --remove-edges.by-vclass pedestrian,rail \
  --try-join-tls --osm-files $OSM_FILE --output-file $SAVEDIR/map.net.xml \
  --remove-edges.isolated;

  python $SUMO_HOME/tools/randomTrips.py -n $SAVEDIR/map.net.xml -e $num_nodes \
  --seed=$RANDOM --fringe-factor 5.0 --intermediate=$RANDOM \
  --trip-attributes='departLane="best" departPos="random_free"' -o $SAVEDIR/map.passenger.trips.xml;

  # # # # Repairing connectivity problems in existing route files
  $SUMO_HOME/bin/duarouter -n $SAVEDIR/map.net.xml -t $SAVEDIR/map.passenger.trips.xml -o $SAVEDIR/route.rou.xml \
  --ignore-errors --repair;

  # Set those placeholders in osm-template.sumocfg
  cp $TA/wizards/osm-template.sumocfg $SAVEDIR/sumocfg.sumocfg;
  sed -i "s/\${net_filename}/map\.net\.xml/" $SAVEDIR/sumocfg.sumocfg;
  sed -i "s/\${route_filename}/route\.rou\.xml/" $SAVEDIR/sumocfg.sumocfg;
  sed -i "s/\${end_time}/$end_time/" $SAVEDIR/sumocfg.sumocfg;

  SCENARIO_TCL_FILENAME=scenario-$num_nodes-$i.tcl;
  ACTIVITY_TCL_FILENAME=activity-$num_nodes-$i.tcl;
  MOBILITY_TCL_FILENAME=mobility-$num_nodes-$i.tcl;
  CONFIG_TCL_FILENAME=config-$num_nodes-$i.tcl;

  # Set those placeholders in scenario tcl
  cp $TA/wizards/template.tcl $SAVEDIR/$SCENARIO_TCL_FILENAME;

  MOBILITY_PLACEHOLDER="\${mobility_filename}";
  ACTIVITY_PLACEHOLDER="\${activity_filename}";
  MOBILITY_TCL_FILENAME_EXT="$SAVEDIR/$MOBILITY_TCL_FILENAME";
  ACTIVITY_TCL_FILENAME_EXT="$SAVEDIR/$ACTIVITY_TCL_FILENAME";
  sed -i "s%$MOBILITY_PLACEHOLDER%$MOBILITY_TCL_FILENAME_EXT%g" $SAVEDIR/$SCENARIO_TCL_FILENAME;
  sed -i "s%$ACTIVITY_PLACEHOLDER%$ACTIVITY_TCL_FILENAME_EXT%g" $SAVEDIR/$SCENARIO_TCL_FILENAME;

  CBRSTART_PLACEHOLDER="\${cbr_start}";
  CBRSTOP_PLACEHOLDER="\${cbr_stop}";
  sed -i "s%$CBRSTART_PLACEHOLDER%$cbrstart%g" $SAVEDIR/$SCENARIO_TCL_FILENAME;
  sed -i "s%$CBRSTOP_PLACEHOLDER%$cbrstop%g" $SAVEDIR/$SCENARIO_TCL_FILENAME;

  # Generate NS2 Mobility file
  sumo -c $SAVEDIR/sumocfg.sumocfg --fcd-output $SAVEDIR/sumo.xml --verbose --summary $SAVEDIR/sumo-running-summary.txt;

  python $SUMO_HOME/tools/traceExporter.py --fcd-input $SAVEDIR/sumo.xml \
   --ns2mobility-output $SAVEDIR/$MOBILITY_TCL_FILENAME \
   --ns2config-output $SAVEDIR/$CONFIG_TCL_FILENAME \
   --ns2activity-output $SAVEDIR/$ACTIVITY_TCL_FILENAME;



  # Comment out lines containing negative numbers in NS2 mobility file
  sed -i '/-/s/^/#/' $SAVEDIR/$MOBILITY_TCL_FILENAME;
  # Comment out lines containing setdest (0.0) (0.0)
  sed -i '/setdest [0-9\.]* 0\.0 /s/^/#/' $SAVEDIR/$MOBILITY_TCL_FILENAME;
  sed -i '/setdest 0\.0 [0-9\.]* /s/^/#/' $SAVEDIR/$MOBILITY_TCL_FILENAME;


  # Change $g to $node_ in activity tcl
  sed -i "s/\$g/\$node_/" $SAVEDIR/$ACTIVITY_TCL_FILENAME;
  # Change stop to reset in activity tcl
  sed -i "s/stop/reset/" $SAVEDIR/$ACTIVITY_TCL_FILENAME;

  # Change load_flatgrid size in scenario tcl
  get_area_length=$(grep -r "opt([x,y])" $SAVEDIR/$CONFIG_TCL_FILENAME);
  # Split string by whitespace and \n
  area_length=($(echo $get_area_length | tr " " "\n"));
  # Set x and y length to load_flatgrid
  LENGTH_X_PLACEHOLDER="\${length_x}";
  LENGTH_Y_PLACEHOLDER="\${length_y}";
  LENGTH_X=$(bc -l <<< "${area_length[2]}+100.0");
  LENGTH_Y=$(bc -l <<< "${area_length[5]}+100.0");

  sed -i "s%$LENGTH_X_PLACEHOLDER%$LENGTH_X%g" $SAVEDIR/$SCENARIO_TCL_FILENAME;
  sed -i "s%$LENGTH_Y_PLACEHOLDER%$LENGTH_Y%g" $SAVEDIR/$SCENARIO_TCL_FILENAME;

  #------------------------------ SIMULATION -----------------------------------
  # Flow: cp AODV -> Make -> Simulate -> cp AODV-PNT -> Make -> Simulate

  # Duplicate scenario tcl to be a "non-sed-ed trace placeholder" copy
  cp $SAVEDIR/$SCENARIO_TCL_FILENAME $SAVEDIR/$SCENARIO_TCL_FILENAME.bak;

  # Simulate original
  # trace filename for AODV (original)
  TRACE_PLACEHOLDER="\${trace_filename}";
  TRACE_FILENAME="$SAVEDIR/trace-$num_nodes-$i.tr";
  sed -i "s%$TRACE_PLACEHOLDER%$TRACE_FILENAME%g" $SAVEDIR/$SCENARIO_TCL_FILENAME;

  yes | cp -rf $AODV_ORIGINAL_DIR/* $NS/aodv;  # Copy and replace current aodv
  rm $NS/aodv/*.o;                        # Delete compiled binary
  echo 'Running Make for AODV...';
  make -C $NS &> /dev/null;
  echo 'Make finished';
  $NS/ns $SAVEDIR/$SCENARIO_TCL_FILENAME; #2> /dev/null; # Run simulation

  # Replace current scenario tcl with non-sed-ed one
  yes | cp -rf $SAVEDIR/$SCENARIO_TCL_FILENAME.bak $SAVEDIR/$SCENARIO_TCL_FILENAME;

  # Simulate PNT
  # trace filename for AODV-PNT
  TRACE_PLACEHOLDER="\${trace_filename}";
  TRACE_FILENAME_PNT="$SAVEDIR/trace-$num_nodes-$i.tr.pnt";
  sed -i "s%$TRACE_PLACEHOLDER%$TRACE_FILENAME_PNT%g" $SAVEDIR/$SCENARIO_TCL_FILENAME;

  yes | cp -rf $AODV_PNT_DIR/* $NS/aodv;
  rm $NS/aodv/*.o;
  echo 'Running Make for AODV-PNT...';
  make -C $NS &> /dev/null;
  echo 'Make finished';
  $NS/ns $SAVEDIR/$SCENARIO_TCL_FILENAME; #2> /dev/null;

  echo " ";
  echo "Simulation for $SAVEDIR/$SCENARIO_TCL_FILENAME finished.";

  echo "Analysing trace files..."
  # -- Packet Delivery Ratio
  # AODV
  get_pdr="$(gawk -f $TA/awk-scripts/pdr.awk $TRACE_FILENAME)"
  pdr_out=($(echo $get_pdr | tr " " "\n"));
  pdr=${pdr_out[0]};

  # AODV-PNT
  get_pdr="$(gawk -f $TA/awk-scripts/pdr.awk $TRACE_FILENAME_PNT)"
  pdr_out=($(echo $get_pdr | tr " " "\n"));
  pdr_pnt=${pdr_out[0]};

  # -- Average End-to-End Delay
  # AODV
  get_e2ed="$(gawk -f $TA/awk-scripts/e2ed.awk $TRACE_FILENAME)"
  e2ed_out=($(echo $get_e2ed | tr " " "\n"));
  e2ed=${e2ed_out[0]};

  # AODV-PNT
  get_e2ed="$(gawk -f $TA/awk-scripts/e2ed.awk $TRACE_FILENAME_PNT)"
  e2ed_out=($(echo $get_e2ed | tr " " "\n"));
  e2ed_pnt=${e2ed_out[0]};

  # -- Routing Overhead
  # AODV
  get_ro="$(gawk -f $TA/awk-scripts/ro.awk $TRACE_FILENAME)"
  ro_out=($(echo $get_ro | tr " " "\n"));
  ro=${ro_out[0]};

  # AODV-PNT
  get_ro="$(gawk -f $TA/awk-scripts/ro.awk $TRACE_FILENAME_PNT)"
  ro_out=($(echo $get_ro | tr " " "\n"));
  ro_pnt=${ro_out[0]};

  # -- RREQ Count
  # AODV
  get_rreq_count="$(gawk -f $TA/awk-scripts/rreq_count.awk $TRACE_FILENAME)"
  rreq_count_out=($(echo $get_rreq_count | tr " " "\n"));
  rreq_count=${rreq_count_out[0]};

  # AODV-PNT
  get_rreq_count="$(gawk -f $TA/awk-scripts/rreq_count.awk $TRACE_FILENAME_PNT)"
  rreq_count_out=($(echo $get_rreq_count | tr " " "\n"));
  rreq_count_pnt=${rreq_count_out[0]};

  # Insert analysed data to DB
  node_category="$num_nodes";
  folder_name="/osm/${num_nodes}nodes/$i";
  sqlite3 $DATABASE "insert into osm_results ( \
        folder_name, category, \
        aodv_pdr,        pnt_pdr, \
        aodv_e2ed,       pnt_e2ed, \
        aodv_ro,         pnt_ro, \
        aodv_rreq_count, pnt_rreq_count ) \
      values ( \
        \"$folder_name\", $num_nodes, \
        $pdr,            $pdr_pnt, \
        $e2ed,           $e2ed_pnt, \
        $ro,             $ro_pnt, \
        $rreq_count,     $rreq_count_pnt \
      );";

  SAVEDIR=$SAVEDIRBASE;

done

# Revert placeholders
# Replace aodv.cc with aodv.cc.bak
yes | cp -rf $AODV_PNT_DIR/aodv.cc.bak $AODV_PNT_DIR/aodv.cc
# Replace template.tcl with template.tcl.bak
yes | cp -rf $TA/wizards/template.tcl.bak $TA/wizards/template.tcl

# ----------------------------- END OF OSM ------------------------------------
