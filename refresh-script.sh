#!/bin/bash

TA=/home/reisuke/TugasAkhir
NS=$TA/ns-allinone-2.35/ns-2.35
SUMO_HOME=$TA/sumo-0.25.0
SCENARIOS=/mnt/local_disk_e/TugasAkhir/scenarios
DATABASE=$SCENARIOS/simulation.db
AODV_ORIGINAL_DIR=$NS/aodv_original_hello
AODV_PNT_DIR=$NS/aodv_pnt

# Replace aodv.cc with aodv.cc.bak
yes | cp -rf $AODV_PNT_DIR/aodv.cc.bak $AODV_PNT_DIR/aodv.cc
# Replace template.tcl with template.tcl.bak
yes | cp -rf $TA/wizards/template.tcl.bak $TA/wizards/template.tcl

rm $SCENARIOS/simulation.db
rm -rf $SCENARIOS/grid/50nodes/*
rm -rf $SCENARIOS/grid/100nodes/*
rm -rf $SCENARIOS/grid/150nodes/*
rm -rf $SCENARIOS/grid/200nodes/*
rm -rf $SCENARIOS/osm/50nodes/*
rm -rf $SCENARIOS/osm/100nodes/*
rm -rf $SCENARIOS/osm/150nodes/*
rm -rf $SCENARIOS/osm/200nodes/*
