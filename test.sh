#!/bin/bash

WIZARDS=/home/reisuke/TugasAkhir/wizards/

echo -e "20\n50\n" | ./automation_grid.sh;
echo -e "20\n100\n" | ./automation_grid.sh;
echo -e "20\n150\n" | ./automation_grid.sh;
echo -e "20\n200\n" | ./automation_grid.sh;
echo -e "10\n50\n" | ./automation_osm.sh;
echo -e "10\n100\n" | ./automation_osm.sh;
echo -e "10\n150\n" | ./automation_osm.sh;
echo -e "10\n200\n" | ./automation_osm.sh;

# Send email upon completion
size_cmd=$(du -sh /mnt/local_disk_e/TugasAkhir/scenarios)
space_cmd=$(df -h | grep /dev/sda8)
size=($(echo $size_cmd | tr " " "\n"))
space=($(echo $space_cmd | tr " " "\n"))
date=$(date)
size=${size[0]}
space=${space[3]}

yes | cp -f $WIZARDS/email-content.txt.bak $WIZARDS/email-content.txt
sed -i "s/\${date_now}/$date/" $WIZARDS/email-content.txt;
sed -i "s/\${total_size}/$size/" $WIZARDS/email-content.txt;
sed -i "s/\${remaining}/$space/" $WIZARDS/email-content.txt;

ssmtp reisuke.raizan@gmail.com < $WIZARDS/email-content.txt

# Shutdown
#/usr/bin/dbus-send --system --print-reply --dest="org.freedesktop.ConsoleKit" /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Stop
