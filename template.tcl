# ===================================================
# Author: REYHAN ARIEF 04/16/2016
# ==================================================

set val(chan) Channel/WirelessChannel ;# channel type
set val(prop) Propagation/TwoRayGround ;# radio-propagation model
set val(netif) Phy/WirelessPhy ;# network interface type
set val(mac) Mac/802_11 ;# MAC type
set val(ifq) Queue/DropTail/PriQueue ;# interface queue type
set val(ll) LL ;# link layer type
set val(ant) Antenna/OmniAntenna ;# antenna model
set val(ifqlen) 50 ;# max packet in ifq
set val(rp) AODV ;# routing protocol
set val(nn) ${num_nodes}
set val(cbrsize) ${cbrsize}    ;# 512 Bytes
set val(cbrrate) ${cbrrate}
set val(cbrinterval) ${cbrinterval}  ;# 1 packet per second
set val(stop) ${end_time}
set val(mobilityfile) "${mobility_filename}"
set val(activityfile) "${activity_filename}"

# Initialize ns
set ns_ [new Simulator]

set tracefd [open ${trace_filename} w]
$ns_ trace-all $tracefd

# Set up topography object
set topo       [new Topography]
$topo load_flatgrid ${length_x} ${length_y}

create-god [expr $val(nn) + 2]
set channel_ [new $val(chan)]
$ns_ node-config \
      -adhocRouting $val(rp) \
      -llType $val(ll) \
      -macType $val(mac) \
      -ifqType $val(ifq) \
      -ifqLen $val(ifqlen) \
      -antType $val(ant) \
      -propType $val(prop) \
      -phyType $val(netif) \
      -channel  $channel_ \
      -agentTrace ON \
      -routerTrace ON \
      -macTrace OFF \
      -movementTrace OFF \
      -topoInstance $topo

for {set i 0} {$i < $val(nn)} {incr i} {
    set node_($i) [$ns_ node]
	  $node_($i) random-motion 0
}

puts "Loading mobility file..."
set where [file dirname [info script]]
source [file join $where $val(mobilityfile)]
source [file join $where $val(activityfile)]

# Stationary nodes
set sender [$ns_ node]
$sender set X_ ${SRC_X}
$sender set Y_ ${SRC_Y}
$sender set Z_ 0.0

set receiver [$ns_ node]
$receiver set X_ ${DST_X}
$receiver set Y_ ${DST_Y}
$receiver set Z_ 0.0

#Setup a UDP connection
set udp [new Agent/UDP]
$ns_ attach-agent $sender $udp
set null [new Agent/Null]
$ns_ attach-agent $receiver $null
$ns_ connect $udp $null

#Setup a CBR over UDP connection
set cbr [new Application/Traffic/CBR]
$cbr attach-agent $udp
$cbr set type_ CBR
$cbr set packet_size_ $val(cbrsize)
$cbr set rate_ $val(cbrrate)
$cbr set interval_ $val(cbrinterval)

#Schedule events for the CBR and FTP agents
$ns_ at 150.0 "$cbr start"
$ns_ at 300.0 "$cbr stop"

# Tell nodes when the simulation ends
for {set i 0} {$i < $val(nn) } {incr i} {
    $ns_ at [expr $val(stop) +1.0] "$node_($i) reset";
}

# Tell static node when simulation ends
$ns_ at [expr $val(stop) +1.0] "$sender reset"
$ns_ at [expr $val(stop) +1.0] "$receiver reset"

# What to do when scenario finished
$ns_ at [expr $val(stop) +1.0] "finish"
$ns_ at [expr $val(stop) +2.0] "puts \"Exiting NS2...\"; $ns_ halt"

proc finish {} {
  global ns_ tracefd
  $ns_ flush-trace
  close $tracefd
}

puts "Starting simulation..."
$ns_ run
